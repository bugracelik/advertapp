package team.pairfy.userapi.infra.api.model;

import lombok.Data;

@Data
public class DeleteUserDto {
    private String mail;
    private String password;
}
