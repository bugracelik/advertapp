package team.pairfy.userapi.infra.api.model;

import java.time.LocalTime;
import java.util.List;


public class FavouriteUserDto {
    private String mail;
    private List<String> favourites;
    private String data = "bunlar random advert id'lerdir.";
    private Integer totalCount;
    private LocalTime time;
    private String acceptLanguage;

    public Integer getTotalCount() {
        return totalCount;
    }

    public String getAcceptLanguage() {
        return acceptLanguage;
    }

    public void setAcceptLanguage(String acceptLanguage) {
        this.acceptLanguage = acceptLanguage;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<String> getFavourites() {
        return favourites;
    }

    public void setFavourites(List<String> favourites) {
        this.favourites = favourites;
    }
}
