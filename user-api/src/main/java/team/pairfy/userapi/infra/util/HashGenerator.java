package team.pairfy.userapi.infra.util;

import org.springframework.util.DigestUtils;

public class HashGenerator {
    public static String hashGenerator(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }
}
