package team.pairfy.userapi.infra.api.model;



public class FavouriteAdvertEvent {
    private String advertId;
    private String mail;


    public FavouriteAdvertEvent(String advertId, String mail) {
        this.advertId = advertId;
        this.mail = mail;
    }

    public String getAdvertId() {
        return advertId;
    }

    public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
