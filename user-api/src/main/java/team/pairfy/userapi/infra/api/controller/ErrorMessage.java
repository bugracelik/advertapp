package team.pairfy.userapi.infra.api.controller;

import team.pairfy.userapi.infra.api.model.FavouriteUserDto;

public class ErrorMessage {
    public String getMessage() {
        return message;
    }

    private final String message;

    public ErrorMessage(String message) {
        this.message = message;
    }
}
