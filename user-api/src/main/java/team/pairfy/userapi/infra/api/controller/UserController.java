package team.pairfy.userapi.infra.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;
import team.pairfy.userapi.application.command.*;
import team.pairfy.userapi.application.query.GetAdvertQuery;
import team.pairfy.userapi.application.query.GetAdvertQueryHandler;
import team.pairfy.userapi.application.query.GetAllFavouritesAdvertsQuery;
import team.pairfy.userapi.application.query.GetAllFavouritesAdvertsQueryHandler;
import team.pairfy.userapi.domain.Advert;
import team.pairfy.userapi.infra.api.model.CreateUserDto;
import team.pairfy.userapi.infra.api.model.DeleteUserDto;
import team.pairfy.userapi.infra.api.model.FavouriteUserDto;
import team.pairfy.userapi.infra.api.model.UpdateUserDto;
import team.pairfy.userapi.infra.couchbase.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    CreateUserCommandHandler createUserCommandHandler;

    @Autowired
    GetAdvertQueryHandler getAdvertQueryHandler;

    @Autowired
    DeleteUserCommandHandler deleteUserCommandHandler;

    @Autowired
    UpdateUserCommandHandler updateUserCommandHandler;

    @Autowired
    FavouriteAddedAdvertCommandHandler handler;

    @Autowired
    GetAllFavouritesAdvertsQueryHandler getAllFavouritesAdvertsQueryHandler;

    @Autowired
    UserRepository repository;

    @PostMapping("create")
    public String createUser(@RequestBody CreateUserDto request) // trafik geldi
    {

        // commandi oluşturdum
        CreateUserCommand command = new CreateUserCommand(request);

        // handle command
        createUserCommandHandler.handle(command);

        return "kayıt edildi";

    }

    @GetMapping("getAdvert/{email}")
    public List<Advert> getAdvertsFromUser(@PathVariable String email) // trafik geldi
    {

        // commandi oluşturdum
        GetAdvertQuery getAdvertQuery = new GetAdvertQuery(email);

        // handle command
        return getAdvertQueryHandler.handle(getAdvertQuery);

    }

    @DeleteMapping("delete")
    public void deleteUser(@RequestBody DeleteUserDto reqeust) {

        //command' oluşturdum
        DeleteUserCommand deleteCommand = new DeleteUserCommand(reqeust);

        //handle command
        deleteUserCommandHandler.handle(deleteCommand);
    }

    @PutMapping("update")
    public void updateUser(@RequestBody UpdateUserDto request) {
        UpdateUserCommand updateCommand = new UpdateUserCommand(request);

        updateUserCommandHandler.handle(updateCommand);

    }

    @PostMapping("add-favorite-advert/{advertId}/{mail}")
    public void foo(@PathVariable String advertId, @PathVariable String mail) throws JsonProcessingException {
        FavouriteAddedAdvertCommand command = new FavouriteAddedAdvertCommand(advertId, mail);

        handler.handle(command);

    }

    @GetMapping("my-favourites-adverts/{mail}")
    public Object bar(@PathVariable String mail, HttpServletRequest request, @RequestHeader("accept-language") String language, @RequestHeader("bugra") String bugra, @RequestHeader("token") String token) {

        String couchbaseToken = repository.getToken();

        if (!token.equals(couchbaseToken))
            return new ErrorMessage("token hatalı");

        GetAllFavouritesAdvertsQuery query = new GetAllFavouritesAdvertsQuery(mail);
        FavouriteUserDto dto = getAllFavouritesAdvertsQueryHandler.handle(query);
        dto.setAcceptLanguage(language);
        return dto;
    }


    // TODO: 14.06.2021 ilanlarım arasında 200 ile 300 arasında alınması. (tek kullanıcı)
    // TODO: 14.06.2021 pasif ilan 6 saat sonra aktif ilan olucak 10 gün sonrada pasife geçicek
    // TODO: 14.06.2021 restfull command and query


}
