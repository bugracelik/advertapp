package team.pairfy.userapi.infra.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.kv.MutationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.util.HashGenerator;

@Component
public class UserRepository {

    @Autowired
    Bucket userBucket;

    public MutationResult save(User user)
    {
        MutationResult result = userBucket.defaultCollection().upsert(user.getId(), user);
        return result;
    }


    public User loadByMail(String email) {
        String emailHash = HashGenerator.hashGenerator(email);
        GetResult getResult = userBucket.defaultCollection().get(emailHash);
        return getResult.contentAs(User.class);
    }

    public void delete(User user) {
        String idOfUser = HashGenerator.hashGenerator(user.getMail());
        userBucket.defaultCollection().remove(idOfUser);
    }

    public void update(User user) {
        userBucket.defaultCollection().upsert(user.getId(), user);
    }

    public String getToken() {
        GetResult token = userBucket.defaultCollection().get("token");
        String contentAs = token.contentAs(String.class);
        return contentAs;
    }
}
