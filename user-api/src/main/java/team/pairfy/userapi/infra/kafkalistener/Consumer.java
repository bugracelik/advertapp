package team.pairfy.userapi.infra.kafkalistener;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.kv.ExistsResult;
import com.couchbase.client.java.kv.GetResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import team.pairfy.userapi.domain.Advert;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.util.HashGenerator;

import java.util.ArrayList;
import java.util.List;

@Component
public class Consumer {

    @Autowired
    Bucket bucket;

    @KafkaListener(id = "deneme", topics = "9ftwl3o5-bugra")
    public void consumer(String message) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Advert advert = objectMapper.readValue(message, Advert.class);

        String mail = advert.getMail();

        String md5DigestAsHexMail = HashGenerator.hashGenerator(mail);

        ExistsResult exists = bucket.defaultCollection().exists(md5DigestAsHexMail);


        if (exists.exists()) {
            GetResult getResult = bucket.defaultCollection().get(md5DigestAsHexMail);
            User user = getResult.contentAs(User.class);
            user.addAdvert(advert);
            bucket.defaultCollection().upsert(md5DigestAsHexMail, user);
        }

    }

}
