package team.pairfy.userapi.infra.api.model;


public class CreateUserDto {
    private String mail;
    private String password; // hash

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
