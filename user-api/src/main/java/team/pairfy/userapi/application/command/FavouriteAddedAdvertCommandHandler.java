package team.pairfy.userapi.application.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.api.model.FavouriteAdvertEvent;
import team.pairfy.userapi.infra.couchbase.UserRepository;


@Component
public class FavouriteAddedAdvertCommandHandler {

    @Autowired
    UserRepository repository;

    @Autowired
    KafkaTemplate kafkaTemplate;
    
    

    public void handle(FavouriteAddedAdvertCommand command) throws JsonProcessingException {
        User user = repository.loadByMail(command.getMail());

        user.addFavouriteAdvert(command.getAdvertId());

        repository.update(user);

        FavouriteAdvertEvent favouriteAdvertDto = new FavouriteAdvertEvent(command.getAdvertId(), command.getMail());
        String favouriteDtoToJson = new ObjectMapper().writeValueAsString(favouriteAdvertDto);

        kafkaTemplate.send("9ftwl3o5-favourite", favouriteDtoToJson);
    }
}
