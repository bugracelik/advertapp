package team.pairfy.userapi.application.query;

public class GetAdvertQuery {
    private String email;

    public GetAdvertQuery(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
