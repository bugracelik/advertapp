package team.pairfy.userapi.application.query;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.userapi.domain.Advert;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.couchbase.UserRepository;

import java.util.List;

@Component
public class GetAdvertQueryHandler {

    @Autowired
    UserRepository repository;

    public List<Advert> handle(GetAdvertQuery query){
        User user = repository.loadByMail(query.getEmail());

        return user.getAdverts();
    }
}
