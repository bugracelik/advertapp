package team.pairfy.userapi.application.query;

public class GetAllFavouritesAdvertsQuery {
    private final String mail;

    public GetAllFavouritesAdvertsQuery(String mail) {
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }
}
