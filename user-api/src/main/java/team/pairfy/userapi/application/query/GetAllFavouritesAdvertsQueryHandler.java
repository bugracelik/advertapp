package team.pairfy.userapi.application.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.api.model.FavouriteUserDto;
import team.pairfy.userapi.infra.couchbase.UserRepository;

import java.time.LocalTime;

@Component
public class GetAllFavouritesAdvertsQueryHandler {

    @Autowired
    UserRepository repository;

    public FavouriteUserDto handle(GetAllFavouritesAdvertsQuery query) {
        User user = repository.loadByMail(query.getMail());


        FavouriteUserDto favouriteUserDto = new FavouriteUserDto();

        favouriteUserDto.setMail(user.getMail());
        favouriteUserDto.setFavourites(user.getFavourites());
        favouriteUserDto.setTotalCount(user.getFavourites().size());
        favouriteUserDto.setTime(LocalTime.now());

        return favouriteUserDto;

    }
}
