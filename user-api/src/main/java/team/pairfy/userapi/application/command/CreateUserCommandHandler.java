package team.pairfy.userapi.application.command;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.couchbase.UserRepository;

@Component
public class CreateUserCommandHandler {

    @Autowired
    UserRepository userRepository;

    public void handle(CreateUserCommand command) {

        // domaine çıktım
        User createdUser = User.create(command.getMail(), command.getPassword());

        userRepository.save(createdUser);
    }


}
