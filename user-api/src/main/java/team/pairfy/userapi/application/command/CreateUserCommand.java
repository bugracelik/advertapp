package team.pairfy.userapi.application.command;

import team.pairfy.userapi.infra.api.model.CreateUserDto;

public class CreateUserCommand {
    private String mail;
    private String password;

    public CreateUserCommand(CreateUserDto request) {
        mail = request.getMail();
        password = request.getPassword();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
