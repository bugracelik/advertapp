package team.pairfy.userapi.application.command;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.couchbase.UserRepository;

@Component
public class UpdateUserCommandHandler {

    @Autowired
    UserRepository repository;

    public void handle(UpdateUserCommand updateCommand) {
        User user = repository.loadByMail(updateCommand.getMail());
        user.setMail(updateCommand.getMail());
        repository.update(user);
    }
}
