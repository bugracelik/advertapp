package team.pairfy.userapi.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.userapi.domain.User;
import team.pairfy.userapi.infra.couchbase.UserRepository;


@Component
public class DeleteUserCommandHandler {

    @Autowired
    UserRepository userRepository;


    public void handle(DeleteUserCommand deleteCommand) {
        User user = User.delete(deleteCommand.getMail(), deleteCommand.getPassword());

        userRepository.delete(user);
    }
}
