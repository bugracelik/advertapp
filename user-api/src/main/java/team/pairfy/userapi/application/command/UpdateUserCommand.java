package team.pairfy.userapi.application.command;

import lombok.Data;
import team.pairfy.userapi.infra.api.model.UpdateUserDto;

@Data
public class UpdateUserCommand {
    private String mail;
    private String password;

    public UpdateUserCommand(UpdateUserDto request) {
        this.mail = request.getMail();
        this.password = request.getPassword();
    }
}
