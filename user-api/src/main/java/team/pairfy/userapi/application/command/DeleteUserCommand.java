package team.pairfy.userapi.application.command;

import lombok.Data;
import team.pairfy.userapi.infra.api.model.DeleteUserDto;


@Data
public class DeleteUserCommand {
    private String mail;
    private String password;

    public DeleteUserCommand(DeleteUserDto reqeust) {
        mail = reqeust.getMail();
        password = reqeust.getPassword();
    }
}
