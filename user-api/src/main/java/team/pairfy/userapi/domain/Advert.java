package team.pairfy.userapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Advert {
    private String id;
    private String text;
    private Double price;
    private String mail;
}
