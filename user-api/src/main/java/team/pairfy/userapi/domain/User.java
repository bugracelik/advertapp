package team.pairfy.userapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.util.DigestUtils;
import team.pairfy.userapi.infra.util.HashGenerator;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    private String id;
    private String mail;
    private String passwordHash;
    private List<Advert> adverts;
    private List<String> favourites;


    private User() {
    }

    private User(String mail, String passwordHash) {
        this.mail = mail;
        this.passwordHash = passwordHash;
    }

    private User(String id, String mail, String passwordHash) {
        this.id = id;
        this.mail = mail;
        this.passwordHash = passwordHash;
    }

    public void addAdvert(Advert advert) {
        adverts.add(advert);
    }

    public static User create(String mail, String password) {
        User user = new User();

        user.setId(DigestUtils.md5DigestAsHex(mail.getBytes()));
        user.setPasswordHash(DigestUtils.md5DigestAsHex(password.getBytes()));
        user.setMail(mail);

        return user;
    }

    public static User delete(String mail, String password) {
        String hashGenerator = HashGenerator.hashGenerator(password);

        return new User(mail, hashGenerator);
    }

    public static User update(String mail, String password) {
        String passwordHash = HashGenerator.hashGenerator(password);
        String idOfUserHash = HashGenerator.hashGenerator(mail);

        return new User(idOfUserHash, mail, passwordHash);
    }

    public void addFavouriteAdvert(String advertId) {
        this.favourites.add(advertId);
    }
}
