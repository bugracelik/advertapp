package pairfy.team.mail.service.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@SpringBootApplication
@RestController
public class MailSenderApplication {

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	ObjectMapper objectMapper;

	public static void main(String[] args) {
		SpringApplication.run(MailSenderApplication.class, args);
	}

	Logger logger = LoggerFactory.getLogger(MailSenderApplication.class);

	@KafkaListener(id = "adana", topics = "9ftwl3o5-deletetopic")
	public void delete(String message) throws JsonProcessingException {
		logger.info("delete");;

		HashMap hashMap = objectMapper.readValue(message, HashMap.class);

		deleteSender(hashMap.get("id"), hashMap.get("text"), hashMap.get("price"));
		logger.info("deleted");
	}

	public void deleteSender(Object id, Object text, Object price) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		simpleMailMessage.setFrom("dropdeneme100@gmail.com");
		simpleMailMessage.setTo("mrbugracelik@gmail.com");

		String message = String.format("%s' numaralı ilanınız sistemimizden silindi", id);

		simpleMailMessage.setText(message);
		simpleMailMessage.setSubject("advert");

		emailSender.send(simpleMailMessage);

	}


	@KafkaListener(id = "test", topics = "9ftwl3o5-bugra")
	public void added(String message) throws JsonProcessingException {
		logger.info("consume");;

		HashMap hashMap = objectMapper.readValue(message, HashMap.class);


		sender(hashMap.get("id"), hashMap.get("text"), hashMap.get("price"), hashMap.get("mail"));
		logger.info("sended");
	}

	@KafkaListener(id = "favourite", topics = "9ftwl3o5-favourite")
	public void foo(String message) throws JsonProcessingException {
		HashMap hashMap = objectMapper.readValue(message, HashMap.class);

		Object advertId = hashMap.get("advertId");
		Object mail = hashMap.get("mail");

		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		simpleMailMessage.setFrom("dropdeneme100@gmail.com");
		simpleMailMessage.setTo(mail.toString());

		String formatMessage = String.format("%s' numaralı ilanı favorilere eklediniz", advertId);
		simpleMailMessage.setText(formatMessage);
		simpleMailMessage.setSubject("favoriler");

		emailSender.send(simpleMailMessage);

	}

	@KafkaListener(id = "favourite-2", topics = "9ftwl3o5-favourite")
	public void bar(String message) throws JsonProcessingException {
		HashMap hashMap = objectMapper.readValue(message, HashMap.class);
		Object advertId = hashMap.get("advertId");
		String url = String.format("http://127.0.0.1:8081/id/%s", advertId);


		HashMap object = new RestTemplate().getForObject(url, HashMap.class);
		Object mail = object.get("mail");

		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		simpleMailMessage.setFrom("dropdeneme100@gmail.com");
		simpleMailMessage.setTo(mail.toString());

		String formatMessage = String.format("%s' numaralı ilanınız başkaları tarafından favorilere eklendi", advertId);
		simpleMailMessage.setText(formatMessage);
		simpleMailMessage.setSubject("favoriler");

		emailSender.send(simpleMailMessage);

	}





	public void sender(Object id, Object text, Object price, Object mail) {
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();

		simpleMailMessage.setFrom("dropdeneme100@gmail.com");
		simpleMailMessage.setTo(mail.toString());

		String message = String.format("%s' numaralı ilanınız sistemimize kayıt edildi", id);

		simpleMailMessage.setText(message);
		simpleMailMessage.setSubject("advert");

		emailSender.send(simpleMailMessage);

	}
}
