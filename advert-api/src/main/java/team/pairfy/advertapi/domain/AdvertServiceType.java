package team.pairfy.advertapi.domain;

public enum AdvertServiceType {
    NORMAL, VITRIN, ACIL
}
