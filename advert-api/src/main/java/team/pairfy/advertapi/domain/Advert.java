package team.pairfy.advertapi.domain;

import lombok.Data;

@Data
public class Advert {
    private String id;
    private String text;
    private Double price;
    private String mail;
    private AdvertServiceType type = AdvertServiceType.NORMAL;
}


