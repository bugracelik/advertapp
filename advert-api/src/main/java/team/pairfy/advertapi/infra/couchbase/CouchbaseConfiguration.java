package team.pairfy.advertapi.infra.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CouchbaseConfiguration {
    @Bean
    public Bucket advertBucket() {
        Cluster cluster = Cluster.connect("46.101.153.130", "Administrator", "123123");

        Bucket bucket = cluster.bucket("Advert");

        return bucket;
    }
}

