package team.pairfy.advertapi.infra.util;

import org.springframework.util.DigestUtils;

public class HashGenerator {
    public static String hashGenerator(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes());
    }
}
