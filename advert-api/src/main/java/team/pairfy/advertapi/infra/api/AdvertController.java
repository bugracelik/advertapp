package team.pairfy.advertapi.infra.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team.pairfy.advertapi.application.command.*;
import team.pairfy.advertapi.application.dto.CreateAdvertDto;
import team.pairfy.advertapi.application.dto.UpdateAdvertDto;
import team.pairfy.advertapi.application.query.GetAdvertBYIdQuery;
import team.pairfy.advertapi.application.query.GetAdvertBYIdQueryHandler;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.infra.couchbase.AdvertRepository;

@RestController
public class AdvertController {

    @Autowired
    GetAdvertBYIdQueryHandler getAdvertBYIdQueryHandler;

    @Autowired
    AdvertRepository advertRepository;

    @Autowired
    CreateAdvertCommandHandler createAdvertCommandHandler;

    @Autowired
    DeleteAdvertCommandHandler deleteAdvertCommandHandler;

    @Autowired
    UpdateAdvertCommandHandler updateAdvertCommandHandler;

    @GetMapping("id/{id}")
    public Advert id(@PathVariable String id) {
        return getAdvertBYIdQueryHandler.handle(new GetAdvertBYIdQuery(id));
    }

    @PostMapping("create/advert")
    public void createAdvert(@RequestBody CreateAdvertDto createAdvertDto) throws JsonProcessingException {
        advertRepository.save(createAdvertDto);
    }


    @PostMapping("command/advert")
    public void createAdvertCommand(@RequestBody CreateAdvertDto request) throws JsonProcessingException {
        CreateAdvertCommand command = new CreateAdvertCommand();

        command.setPrice(request.getPrice());
        command.setText(request.getText());
        command.setType(request.getType());
        command.setMail(request.getMail());

        createAdvertCommandHandler.handle(command);
    }

    @DeleteMapping("command/delete/{id}")
    public void deleteAdvertCommand(@PathVariable String id) throws JsonProcessingException {
        DeleteAdvertCommand deleteAdvertCommand = new DeleteAdvertCommand();

        deleteAdvertCommand.setId(id);

        deleteAdvertCommandHandler.handle(deleteAdvertCommand);
    }


    @PutMapping("update")
    public void updateAdvert(@RequestBody UpdateAdvertDto request) {
        UpdateAdvertCommand updateAdvertCommand = new UpdateAdvertCommand(request);

        updateAdvertCommandHandler.handle(updateAdvertCommand);

    }




}
