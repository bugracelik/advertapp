package team.pairfy.advertapi.infra.couchbase;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.json.JsonObject;
import com.couchbase.client.java.kv.ExistsResult;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.kv.MutationResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.application.dto.CreateAdvertDto;
import team.pairfy.advertapi.infra.util.HashGenerator;

import java.util.UUID;

@Component
public class AdvertRepository {

    @Autowired
    Bucket advertBucket;

    @Autowired
    KafkaTemplate kafkaTemplate;

    @Autowired
    ObjectMapper objectMapper;

    public Advert load(String id)
    {
        GetResult getResult = advertBucket.defaultCollection().get(id);
        return getResultToAdvert(getResult);
    }

    private Advert getResultToAdvert(GetResult getResult) {
        return getResult.contentAs(Advert.class);
    }

    public void save(CreateAdvertDto createAdvertDto) throws JsonProcessingException {
        Advert advert = new Advert();

        advert.setPrice(createAdvertDto.getPrice());
        advert.setText(createAdvertDto.getText());
        advert.setMail(createAdvertDto.getMail());

        String generateId = UUID.randomUUID().toString();
        advert.setId(generateId);


        MutationResult insert = advertBucket.defaultCollection().insert(generateId, advert);

        String advertJson = objectMapper.writeValueAsString(advert);


        kafkaTemplate.send("9ftwl3o5-bugra", advertJson);


    }

    public void save(Advert advert) throws JsonProcessingException {

        String generateId = UUID.randomUUID().toString();
        advert.setId(generateId);

        MutationResult response = advertBucket.defaultCollection().insert(generateId, advert);


        String advertJson = objectMapper.writeValueAsString(advert);


        kafkaTemplate.send("9ftwl3o5-bugra", advertJson);


    }


    public void delete(Advert advert) throws JsonProcessingException {

        advertBucket.defaultCollection().remove(advert.getId());

        String advertJson = objectMapper.writeValueAsString(advert);

        kafkaTemplate.send("9ftwl3o5-deletetopic", advertJson);
    }

    public void update(Advert advert) {
        String id = advert.getId();
        Collection bucketCollection = advertBucket.defaultCollection();

        if (bucketCollection.exists(id).exists()) {
            bucketCollection.upsert(id, advert);
        }
        else {
            bucketCollection.insert(id, advert);
        }


    }
}
