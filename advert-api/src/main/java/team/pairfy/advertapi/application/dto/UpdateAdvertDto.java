package team.pairfy.advertapi.application.dto;


import lombok.Data;

@Data
public class UpdateAdvertDto {
    private String id;
    private String text;
    private Double price;
    private String mail;
}
