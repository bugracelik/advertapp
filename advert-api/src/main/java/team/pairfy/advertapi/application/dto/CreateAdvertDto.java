package team.pairfy.advertapi.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import team.pairfy.advertapi.domain.AdvertServiceType;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateAdvertDto {
    private String text;
    private Double price;
    private String mail;
    private AdvertServiceType type;
}
