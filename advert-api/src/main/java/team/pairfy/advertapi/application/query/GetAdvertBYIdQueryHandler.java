package team.pairfy.advertapi.application.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.infra.couchbase.AdvertRepository;

@Component
public class GetAdvertBYIdQueryHandler {

    @Autowired
    AdvertRepository repository;

    public Advert handle(GetAdvertBYIdQuery query) {

        Advert advert = repository.load(query.getId());

        return advert;
    }

}
