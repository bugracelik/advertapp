package team.pairfy.advertapi.application.command;


import lombok.Data;
import team.pairfy.advertapi.domain.AdvertServiceType;

@Data
public class CreateAdvertCommand {
    private String text;
    private Double price;
    private AdvertServiceType type;
    private String mail;

}
