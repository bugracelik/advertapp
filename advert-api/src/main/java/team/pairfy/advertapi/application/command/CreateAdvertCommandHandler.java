package team.pairfy.advertapi.application.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.infra.couchbase.AdvertRepository;

@Component
public class CreateAdvertCommandHandler {

    @Autowired
    AdvertRepository advertRepository;

    public void handle(CreateAdvertCommand command) throws JsonProcessingException {
        Advert advert = new Advert();

        advert.setPrice(command.getPrice());
        advert.setText(command.getText());
        advert.setType(command.getType());
        advert.setMail(command.getMail());

        advertRepository.save(advert);
    }
}
