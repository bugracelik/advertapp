package team.pairfy.advertapi.application.command;


import lombok.Data;

@Data
public class DeleteAdvertCommand {
    private String id;
    private String text;
    private String mail;
    private Double price;
}
