package team.pairfy.advertapi.application.query;

import lombok.Data;

@Data
public class GetAdvertBYIdQuery implements Query {
    private String id;

    public GetAdvertBYIdQuery(String id) {
        this.id = id;
    }
}


