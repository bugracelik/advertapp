package team.pairfy.advertapi.application.command;

import lombok.Data;
import team.pairfy.advertapi.application.dto.UpdateAdvertDto;

@Data
public class UpdateAdvertCommand {
    private String id;
    private String text;
    private Double price;
    private String mail;

    public UpdateAdvertCommand(UpdateAdvertDto request) {
        this.id = request.getId();
        this.text = request.getText();
        this.price = request.getPrice();
        this.mail = request.getMail();
    }
}
