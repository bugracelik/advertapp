package team.pairfy.advertapi.application.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.infra.couchbase.AdvertRepository;


@Component
public class UpdateAdvertCommandHandler {

    @Autowired
    AdvertRepository advertRepository;

    public void handle(UpdateAdvertCommand updateAdvertCommand) {
        Advert advert = new Advert();

        advert.setMail(updateAdvertCommand.getMail());
        advert.setPrice(updateAdvertCommand.getPrice());
        advert.setText(updateAdvertCommand.getText());
        advert.setId(updateAdvertCommand.getId());

        advertRepository.update(advert);
    }
}
