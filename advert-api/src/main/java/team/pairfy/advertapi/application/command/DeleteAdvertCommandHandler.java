package team.pairfy.advertapi.application.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team.pairfy.advertapi.domain.Advert;
import team.pairfy.advertapi.infra.couchbase.AdvertRepository;

@Component
public class DeleteAdvertCommandHandler {

    @Autowired
    AdvertRepository advertRepository;

    public void handle(DeleteAdvertCommand command) throws JsonProcessingException {
        Advert advert = new Advert();

        advert.setId(command.getId());

        advertRepository.delete(advert);
    }
}
